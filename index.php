<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8 />
	<title>Shaun Gish - Local 600 D.I.T. / Data Manager - Dallas, TX</title>
	<link rel="shortcut icon" href="/favicon.ico?v=2" />
	<link rel="stylesheet" type="text/css" media="screen" href="/base.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="/grid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="/elements.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="/layout.css" />
	<link rel="stylesheet" type="text/css" href="print.css" media="print" />
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:400,700">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300,700">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700">
  <link rel="stylesheet" href="/font-awesome.min.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script type="text/javascript" src="/js/app.js"></script>
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body ng-app="myApp">
  <div id="wrap">
  		<div id="header">
  </div>
    
  <div class="sticky-wrapper dontprint"><div id="nav" class="stuck">

                  <div class="row">
                      <div class="span3">

                          <!-- // Logo // -->
                          <a href="index.html" id="logo">
                              Shaun Gish<br>
                              <small><em>Digital Imaging Technician</em></small>
                          </a>

                      </div><!-- end .span3 -->
                      <div class="span9">

                          <a href="#" id="mobile-menu-trigger" class="mobile-menu-closed">
                              <i class="fa fa-bars"></i>
                          </a>

                          <!-- // Menu // -->
                          <ul class="sf-menu sf-js-enabled" id="menu">
                              <li>
                                  <a href="#profile">Profile</a>
                              </li>
                              <li class="">
                                  <a href="#narrative">Narrative</a>
                              </li>
                              <li>
                                  <a href="#shorts">Short Form</a>
                              </li>
                              <li>
                                  <a href="#commercial">Commercial</a>
                              </li>
                              <li>
                                  <a href="#kit">Kit &amp; Gear</a>
                              </li>
                              <li>
                                  <a href="#contact">Contact</a>
                              </li>
                          </ul>

                      </div><!-- end .span9 -->
                  </div><!-- end .row -->	

  			</div></div>
  
  <div id="content">
    
    <a href="#" id="profile"></a>
    <div id="profile">

      <div class="row cv-section">
        <div class="span3">
	
			<div class="print" id="resume-header">
            	<h1>Shaun Gish</h1>
									<p><strong>IATSE Local 600 D.I.T.</strong><br>
										214.636.1616<br>
									shaungish@gmail.com<br>
									www.shaungish.com</p>
				
            </div>
	
          <div class="cv-section-title">
            <h1><span>Experience<small>Industry Profile</small></span></h1>
              <h3></h3>
              <p></p>
          </div><!-- end .cv-section-title -->
        </div><!-- end .span3 -->
        
        <div class="span9">
            <div class="cv-item">
                
                <div class="row">
                  

                  <p>
                    <ul class="no-style">
                      <li>Texas based <strong>ICG Local 600 Digital Imaging Technician</strong> specializing in commercial workflows.</li>
                      <li>Comprehensive gear list including RED Dragon camera, gimbals, heavy lift drones &amp; wireless video.</li>
                      <li>Over fifteen years of film, broadcast and interactive experience.</li>
                    </ul>
                  </p>
            
                <div class="span4">
                  <h3 class="text-uppercase">Extensive Experience With</h3>
                  <ul>
                    <li>Live On-Set Color Correction</li>
                    <li>Dailies Transcoding</li>
                    <li>Data Management &amp; Checksumming</li>
                    <li>Workflow Design</li>
                    <li>Arri Digital Cameras (including ArriRaw / OpenGate)</li>
                    <li>RED Digital Cameras</li>
                    <li>Canon Cameras</li>
                    <li>Sony Cameras</li>
                    <li>Panasonic Cameras</li>
                    <li>Niche Cameras (GoPro, Si2K, Novo)</li>
                    <li>Gimbals (Movi, Ronin)</li>
                    <li>Wireless Video Systems</li>
                    <li>Avid, Final Cut Pro, Adobe Creative Cloud</li>
                    <li>DaVinci Resolve, Scratch</li>
                  </ul>
                </div>
                
                <div class="span4">
                  <h3 class="text-uppercase">Notable Directors of Photography</h3>

                  <ul id="notable-dps">
	                    <li><a href="http://www.imdb.com/name/nm0278475" target="_blank">Mauro Fiore, A.S.C.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0066244" target="_blank">Dion Beebe, A.S.C., A.C.S.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0907999" target="_blank">Mandy Walker, A.S.C., A.C.S.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0002336" target="_blank">Declan Quinn, A.S.C.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0889678" target="_blank">Checco Varese, A.S.C.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0475578" target="_blank">Ellen Kuras, A.S.C.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0002681" target="_blank">Jim Denault, A.S.C.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0005856" target="_blank">Theo van de Sande, A.S.C.</a></li>
                    <li><a href="http://www.imdb.com/name/nm0914433" target="_blank">Colin Watkinson</a></li>
                    <li><a href="http://www.imdb.com/name/nm0415315" target="_blank">Igor Jadue-Lillo</a></li>
                    <li><a href="http://www.imdb.com/name/nm1086687" target="_blank">Masanobu Takayanagi</a></li>
                    <li><a href="http://www.imdb.com/name/nm0177906" target="_blank">Brown Cooper</a></li>
                    <li><a href="http://www.imdb.com/name/nm0163136" target="_blank">David Claessen</a></li>
                  </ul>
                </div>
                
              </div>
 
               <div class="span8">
                   <h3 class="text-uppercase">Repeat Clients</h3>
                   <p class="client-list">Directorz &bull; Lucky21 &bull; ReelFX &bull; Icognito &bull; Bunker &bull; The Department &bull; HIT Entertainment &bull; Dolby Labs &bull; Nametag Films &bull; Liberal Media Films &bull; Heresay &bull; Missing Pieces &bull; Superlounge &bull; Partizan &bull; O Positive &bull; Anonymous Content &bull; Sugar Film Production &bull; Fueld Films &bull; BOB Industries &bull; Doner &bull; A/V Squad</p>
                 </div>
               
                  
            </div><!-- end .cv-item -->
            <a href="#" id="narrative"></a>
        </div><!-- end .span9 -->
      </div><!-- end .cv-section -->

<div class="print-break"></div>
  
</div>

    <div id="work-experience">

      <div class="row cv-section" id="narrative-list">
        <div class="span3">
          <div class="cv-section-title">
            <h1><span>Narrative<small>Features &amp; Episodic</small></span></h1>
              <h3></h3>
              <p></p>
          </div><!-- end .cv-section-title -->
        </div><!-- end .span3 -->
        <div class="span9">
            <div class="cv-item">
              <h3 class="text-uppercase dontprint">2015</h3>
                
                <p><strong>Hulu Miniseries: "11/22/63"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt2699128/"><img src="/img/imdb_sm.gif"></a><br>
                   Aerial Camera Operator (Dallas Visual Effects Unit)<br>
                </p>
                
                <p><strong>HBO Television: "The Leftovers" ep. 208 &amp; 210</strong> <a class="dontprint" href="http://www.imdb.com/title/tt2699128/"><img src="/img/imdb_sm.gif"></a><br>
                   DIT (2nd Unit) &bull; Downloading, Live Grading<br>
                   Director of Photography: Michael Grady<br>
		               <span class="btn">Arri Alexa (2)</span>
                </p>
                
                <p><strong>Feature: "Miracles From Heaven"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt4257926/"><img src="/img/imdb_sm.gif"></a><br>
                   DIT (Ft. Worth Unit) &bull; Downloading, Color Correction<br>
                   Director of Photography: Checco Varese, A.S.C.<br>
		               <span class="btn">Arri Alexa XT (2)</span>
                </p>
                
                <p><strong>Concert &amp; Documentary: "State Farm Neighborhood Sessions: Toby Keith"</strong><br>
                   DIT &bull; Downloading, Color Correction<br>
                   Director of Photography: Declan Quinn, A.S.C.<br>
		               <span class="btn">Arri Alexa Mini</span><span class="btn">Arri Alexa (5)</span><span class="btn">Arri Alexa Amira (2)</span><span class="btn">Black Magic Pocket Camera (4)</span>
                </p>
                
                <p><strong>Feature: "Truth"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt3859076/"><img src="/img/imdb_sm.gif"></a><br>
                   DIT (Dallas Unit) &bull; Downloading, Transcoding, Color Correction<br>
                   Director of Photography: Mandy Walker, A.S.C., A.C.S.<br>
		               <span class="btn">Arri Alexa XT</span>
                </p>
                 
                <p><strong>HBO Pilot: "Mamma Dallas"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt4427986/"><img src="/img/imdb_sm.gif"></a><br>
                   Data Manager &bull; Downloading, Digital Utility<br>
                   Director of Photography: Jim Denault, A.S.C.<br>
		               <span class="btn">Arri Alexa (2)</span>
                </p>
              
               <h3 class="text-uppercase dontprint">2014</h3>
                <p><strong>Feature: "Windsor"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt3701460/"><img src="/img/imdb_sm.gif"></a><br>
                   Aerial Camera Operator<br>
		               <span class="btn">Panasonic GH4</span>
                </p>

                <p><strong>MTV: "Rebel Music - Austin"</strong><br>
                   Data Manager &amp; 2nd Assistant Camera<br>
		               <span class="btn">Canon C300 (3)</span> 
                </p>
              
                 <p><strong>Documentary: Danny Thompson Land Speed Record</strong><br>
                     Aerial Camera Operator<br>
  		               <span class="btn">Panasonic GH4</span> 
                  </p>
              
                  <p><strong>Documentary: Porsche NTG Racing</strong><br>
                     Aerial Camera Operator<br>
  		               <span class="btn">Panasonic GH4</span> 
                  </p>
              
               <h3 class="text-uppercase dontprint">2012</h3>

               <p><strong>Feature: "Bad Kids Go To Hell"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt1865573/"><img src="/img/imdb_sm.gif"></a><br>
                  DIT (Additional Photography) &bull; Downloading, Transcoding<br>
		              <span class="btn">RED Epic MX</span>
               </p>
               
               <h3 class="text-uppercase dontprint">2011</h3>

                <p><strong>Angelina Ballerina: "Dizzy Feet PSA - Larry King"</strong><br>
                    Producer &bull; Editor &bull; Compositor
                 </p>
               
               <h3 class="text-uppercase dontprint">2010</h3>

               <p><strong>Dolby Laboratories: "CES Show Reel"</strong><br>
                   Producer
                </p>

                 <p><strong>Angelina Ballerina: "Dizzy Feet PSA - Nigel Lythgoe &amp; Carrie Ann Inaba"</strong><br>
                     Producer &bull; Editor &bull; Compositor
                  </p>
                
                 <h3 class="text-uppercase dontprint">2009</h3>

               <p><strong>CMT Pilot: "The Kilroy Show"</strong><br>
                  Producer &bull; Colorist &bull; Post Production Coordinator
               </p>

               <p><strong>Television: "Barney &amp; Friends"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt0144701/"><img src="/img/imdb_sm.gif"></a><br>
                  Editor &bull; <a href="http://www.imdb.com/title/tt1356298/">"The Good Egg: Kenya"</a>, <a class="dontprint" href="http://www.imdb.com/title/tt1356294/">"Big Brother Rusty: China"</a><br>
                  Visual Effects &bull; <a href="http://www.imdb.com/title/tt1356300/">"Venice, Anyone?: Italy"</a><br>
               </p>
               
            </div><!-- end .cv-item -->
            <a href="#" id="shorts"></a>
        </div><!-- end .span9 -->
      </div><!-- end .cv-section -->

      <div class="row cv-section view" id="short-list">
        <div class="span3">
          <div class="cv-section-title">
            <h1><span>Short Form<small>Music Videos &amp; Short Films</small></span></h1>
              <h3></h3>
              <p></p>
          </div><!-- end .cv-section-title -->
        </div><!-- end .span3 -->
        <div class="span9">
            <div class="cv-item">
                <p><strong>Short Film: "Dig"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt3410858/"><img src="/img/imdb_sm.gif"></a><br>
                   Producer &bull; Location Sound Mixer &bull; Assistant Editor<br>
                   <img class="btn" style="margin-bottom: 0" src="/img/sundance_sm.png">
                   <img class="btn" style="margin-bottom: 0" src="/img/ashland_sm.png">
                   <img class="btn" style="margin-bottom: 0" src="/img/diff_sm.png">
                   <img class="btn" style="margin-bottom: 0" src="/img/sxsw_sm.png"><br>
                   Nominations: <span class="btn">2014 Sundance Film Festival Grand Jury Prize Nominee</span><span class="btn">2014 SXSW Film Festival Grand Jury Award Nominee</span>
				   <br><span class="print">2014 Sundance Film Festival Grand Jury Prize</span><br><span class="print">2014 SXSW Film Festival Grand Jury Award</span>
                  </p>
                
                <p><strong>Eli Young Band: "Say Goodnight"</strong><br>
                   DIT &bull; Downloading, Transcoding, Color Correction<br>
                   <span class="btn">RED Epic MX</span>
                  </p>

                   <p><strong>Blue October: "Bleed Out"</strong><br>
                      DIT &bull; Downloading, Transcoding, Color Correction<br>
                      <span class="btn">RED Epic MX</span><span class="btn">Canon 5DmIII</span>
                   </p>

                   <p><strong>The Civil Wars: "The One That Got Away"</strong><br>
                     DIT &bull; Downloading, Transcoding, Color Correction<br>
                     <span class="btn">Arri Alexa XT</span>
                   </p>
        
                   <p><strong>Short Film: "Aftershocks"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt2148825/"><img src="/img/imdb_sm.gif"></a><br>
                      Producer<br>
                     </p>
                     
                     <p><strong>Short Film: "Fight The Foot"</strong> <a class="dontprint" href="http://www.imdb.com/title/tt1863237/"><img src="/img/imdb_sm.gif"></a><br>
                        Producer &bull; Colorist<br>
                       </p>
                     
                   <p><strong>Andrew Tinker: "B Sweet"</strong><br>
                     Producer
                   </p>
		               
            </div><!-- end .cv-item -->
            <a href="#" id="commercial"></a>
            
        </div><!-- end .span9 -->
      </div><!-- end .cv-section -->

      <div class="row cv-section" id="commercial-list">
        <div class="span3">
          <div class="cv-section-title">
            <h1><span>Commmercial<small>2015</small></span></h1>
              <h3></h3>
              <p></p>
          </div><!-- end .cv-section-title -->
        </div><!-- end .span3 -->
        <div class="span9">
            <div class="cv-item">
        
        <h3 class="text-uppercase dontprint">2015</h3>
				
				        <p><strong>Applebee's #397-15</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">Arri Alexa</span>              
                </p>
                
                </p><p><strong>Air Force - Recruiting Commercials</strong><br>
                   Data Manager &bull; Gimbal Operator (Movi) &bull; Focus Puller &bull; Pilot<br>
		               <span class="btn">RED Epic Dragon (2)</span><span class="btn">Canon 5Dm3 (2)</span><span class="btn">GoPro Hero 4 Black (2)</span><span class="btn">DJI Inspire 1</span>           
                </p>
                
                <p><strong>Toyota - Dude Perfect and the 2016 Tacoma</strong><br>
                   Aerial Camera Operator &bull; Pilot<br>
		               <span class="btn">DJI Inspire 1</span><span class="btn">Vulcan Octocopter</span><span class="btn">Canon 5Dm3</span>              
                </p>
                
                <p><strong>Samsung #914</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">Arri Alexa (2)</span>              
                </p>
                
                <p><strong>Texas Water Safari - Yeti Coolers</strong><br>
                   Aerial Camera Operator<br>
		               <span class="btn">DJI Inspire 1</span>              
                </p>
                
                <p><strong>Top Golf</strong><br>
                   Data Manager<br>
		               <span class="btn">RED Epic Dragon</span>              
                </p>
                
                <p><strong>Fox Sports - "Bill Fong Feature"</strong><br>
                   Gimbal Operator (Movi / Tero)<br>
		               <span class="btn">RED Epic Dragon</span>               
                </p>
                
                <p><strong>East Texas Medical Center Commercial</strong><br>
                   Data Manager<br>
		               <span class="btn">RED Epic Dragon (2)</span><span class="btn">Canon 5Dm3 (2)</span>               
                </p>
                
                <p><strong>Hasbro NERF Rival "Win the Day"</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">RED Epic Dragon (2)</span><span class="btn">GoPro Hero 4 Black (2)</span>               
                </p>
                
                <p><strong>American Airlines #I-1506</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">RED Epic Dragon</span>
                </p>
                
                <p><strong>Cici's Pizza</strong><br>
                   Data Manager<br>
		               <span class="btn">Arri Alexa</span>
                </p>
                
                <p><strong>Reliant Energy #15-1815</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">Arri Alexa (2)</span><span class="btn">GoPro Hero 4 Black</span>
                </p>
                
                <p><strong>Discovery #2123: "Dallas Branding"</strong><br>
                   DIT &bull; Downloading, Live Grading<br>
		               <span class="btn">Arri Amira</span><span class="btn">Canon C500</span><span class="btn">Sony a7S (2)</span> 
                </p>
                
                <p><strong>Nissan #042315: "Juke, Qashqai, X Trail"</strong><br>
		               DIT &bull; Downloading, Transcoding, Live Grading<br>
		               Director of Photography: Khalid Mohtaseb<br>
		               <span class="btn">Arri Alexa XT (2)</span><span class="btn">RED Epic Dragon (2)</span><span class="btn">Sony a7S</span><span class="btn">Atomos Shogun</span>
                </p>
                
                <p><strong>Taco Bueno Exterior Plates</strong><br>
		               Director of Photography<br>
		               <span class="btn">RED Epic Dragon</span>
                </p>
                
                <p><strong>Weber Grills #RR0415</strong><br>
		               Data Manager &bull; Downloading, 2nd AC<br>
		               Director of Photography: David G. Wilson<br>
		               <span class="btn">Arri Alexa</span><span class="btn">Black Magic Pocket Camera</span>
                </p>
                
                <p><strong>Six Flags / Coca-Cola</strong><br>
		               DIT &bull; Downloading, Transcoding<br>
		               <span class="btn">GoPro Hero4 (7)</span><span class="btn">Sony NEX-FS700</span><span class="btn">Canon C300 (2)</span><span class="btn">Sony a7S</span>
                </p>
                
                <p><strong>AT&amp;T #15-067: "First Female Pitcher"</strong><br>
		               DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">Arri Alexa (3)</span>
                </p>
                
                 <p><strong>Texas Lotto #501: "Zero Factory $500 Million Cash"</strong><br>
  		             Drone Camera Operator<br>
  		             <span class="btn">RED Epic Dragon</span>
                 </p>
                 
                 <p><strong>Vodafone: "Alan Bean"</strong><br>
   		             DIT &bull; Downloading, Transcoding, Live Grading<br>
   		             <span class="btn">Arri Alexa XT (2)</span><span class="btn">Canon C300</span>
                 </p>
                 
                 <p><strong>Stage Stores #31104</strong><br>
     		           DIT &bull; Downloading, Transcoding<br>
   		             <span class="btn">Arri Alexa</span>
                 </p>
                 
                 <p><strong>JCPenney 460</strong><br>
     		           DIT &bull; Downloading, Transcoding, Live Grading<br>
   		             <span class="btn">Arri Alexa (2)</span>
                 </p>

                 <p><strong>Rooms To Go #21403: </strong><br>
     		           DIT &bull; Downloading, Transcoding, Live Grading<br>
   		             <span class="btn">Arri Alexa (2)</span>
                 </p>

                 <p><strong>American Country Music Awards 2015 Promo</strong><br>
     		           DIT &bull; Downloading, Transcoding, Live Grading<br>
   		             <span class="btn">Arri Alexa (2)</span>
                 </p>

                 <p><strong>Universal Studios Commercial</strong><br>
     		           Movi Camera Operator<br>
   		             <span class="btn">RED Epic Dragon</span>
                 </p>

                 <p><strong>Beats By Dr. Dre: "Dez Bryant"</strong><br>
     		           DIT &bull; Downloading, Transcoding<br>
   		             <span class="btn">Arri Alexa M</span>
                 </p>

                <h3 class="text-uppercase dontprint">2014</h3>
 

                <p><strong>Gold Bond</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
		               <span class="btn">Arri Alexa</span> 
                </p>
                
                <p><strong>Discovery #1971: "Dallas Branding"</strong><br>
                   Aerial Camera Operator<br>
		               <span class="btn">Canon 5DmIII</span> 
                </p>
             
                <p><strong>AT&amp;T: "Spread the Word"</strong><br>
                   VFX Director of Photography &amp; DIT &bull; Downloading, Transcoding, Live Grading<br>
                   Director of Photography: Mauro Fiore, A.S.C.<br>
		               <span class="btn">Arri Alexa (2)</span><span class="btn">RED Epic Dragon</span> 
                </p>
                
                <p><strong>Porsche: "Mirrors"</strong><br>
                   Aerial Camera Opterator &amp; DIT &bull; Downloading, Transcoding<br>
                   Director of Photography:  Mirko Demschick<br>
		               <span class="btn">Arri Alexa XT - Open Gate</span><span class="btn">RED Epic Dragon</span> 
                </p>
                
                <p><strong>TXU Commercial</strong><br>
                   Movi Camera Operator<br>
		               <span class="btn">RED Epic Dragon</span> 
                </p>
                
                <p><strong>Foot Locker: "Excited"</strong><br>
                   Movi Camera Operator<br>
                   Director of Photography: Marc Laliberte Else<br>
		               <span class="btn">Arri Alexa</span> 
                </p>

                <p><strong>JCPenney: "Jingle Brigade"</strong><br>
                   DIT &bull; Downloading, Transcoding, Live Grading<br>
                   Director of Photography: Ellen Kuras A.S.C.<br>
		               <span class="btn">Arri Alexa (2)</span> 
                </p>

                <p><strong>John Deere #4062</strong><br>
                   DIT &bull; Downloading, Transcoding, Color Correction<br>
                   Director of Photography: Dion Beebe, A.C.S., A.S.C.<br>
		               <span class="btn">Arri Alexa (2)</span> 
                </p>

                <p><strong>Muller #BFFSS-065-14: Milchreis</strong><br>
                   DIT &bull; Downloading, Transcoding, Color Correction<br>
		               <span class="btn">RED Epic Dragon (2)</span> 
                </p>

                <p><strong>Charter Communications Commercial</strong><br>
                   Movi Camera Operator<br>
		               <span class="btn">Arri Alexa (2)</span><span class="btn">RED Epic Dragon</span> 
                </p>

                <p><strong>Scottish Rite Hospital PSA</strong><br>
                   Data Manager &amp; Video Assist Operator<br>
		               <span class="btn">RED Epic Dragon</span> 
                </p>
                            
            </div><!-- end .cv-item -->
            <a href="#" id="kit"></a>
        </div><!-- end .span9 -->
      </div><!-- end .cv-section -->    
    
      <div class="row cv-section dontprint">
        <div class="span3">
          <div class="cv-section-title">
            <h1><span>Equipment<small>DIT &amp; Camera</small></span></h1>
              <h3></h3>
              <p></p>
          </div><!-- end .cv-section-title -->
        </div><!-- end .span3 -->
        <div class="span9">
            <div class="cv-item">
              <h3 class="text-uppercase">DIT Base Kit</h3>
              
              <p><strong>Hardware</strong><br>
                 <ul>
                  <li style="margin-bottom: 0" class="btn">2014 8 Core Mac Pro</li>
                  <li style="margin-bottom: 0" class="btn">Flanders Scientific CM-171 17" 10-Bit LCD Monitors (2)</li>
                  <li style="margin-bottom: 0" class="btn">Tangent Element Panel</li>
                  <li style="margin-bottom: 0" class="btn">12TB Promise Pegasus2 R6 Thunderbolt 2 RAID</li>
                  <li style="margin-bottom: 0" class="btn">RED Mini-Mag Reader</li>
                  <li style="margin-bottom: 0" class="btn">Sonnet SxS Reader</li>
                  <li style="margin-bottom: 0" class="btn">Decimator 1:6 SDI Distribution Amplifiers (2)</li>
                  
                 </ul>
              </p>

              <p><strong>Software</strong><br>
                 <ul>
                  <li style="margin-bottom: 0" class="btn">DaVinci Resolve (Full)</li>
                  <li style="margin-bottom: 0" class="btn">Scratch</li>
                  <li style="margin-bottom: 0" class="btn">Pomfort Silverstack</li>
                  <li style="margin-bottom: 0" class="btn">Pomfort LiveGrad Pro</li>
                  <li style="margin-bottom: 0" class="btn">Adobe Creative Cloud</li>
                  <li style="margin-bottom: 0" class="btn">Avid Media Composer</li>
                  <li style="margin-bottom: 0" class="btn">REDCine-X Pro</li>
                </ul>
              </p>
                
              <h3 class="text-uppercase">As Used</h3>              
                 <ul>
                  <li style="margin-bottom: 0" class="btn">Live Color Kit (2 Cameras)</li>
                  <li style="margin-bottom: 0" class="btn">Wireless Director's Monitor</li>
                  <li style="margin-bottom: 0" class="btn">RED Epic Dragon Camera Package</li>
                  <li style="margin-bottom: 0" class="btn">RED Rocket-X (Thunderbolt 2 Enclosure)</li>
                  <li style="margin-bottom: 0" class="btn">Teradek Bolt Pro Wireless Video (3)</li>
                  <li style="margin-bottom: 0" class="btn">Paralinx Tomahawk Long Range Wireless Video</li>
                  <li style="margin-bottom: 0" class="btn">Heden Carat Wireless Follow Focus Kit</li>
                  <li style="margin-bottom: 0" class="btn">Hocus Products Axis One Wireless Follow Focus Kit</li>
                  <li style="margin-bottom: 0" class="btn">SmallHD 502 Monitor</li>
                  <li style="margin-bottom: 0" class="btn">SmallHD AC7 Monitor</li>
                  <li style="margin-bottom: 0" class="btn">SmallHD DP4 Monitor</li>
                  <li style="margin-bottom: 0" class="btn">Atomos Samurai Blade Recorder</li>
                  <li style="margin-bottom: 0" class="btn">HP Ultrium LTO Drive</li>
                  <li style="margin-bottom: 0" class="btn">Freefly Movi M5</li>
                  <li style="margin-bottom: 0" class="btn">Freefly Movi M10</li>
                  <li style="margin-bottom: 0" class="btn">Freefly Movi M15</li>
                  <li style="margin-bottom: 0" class="btn">Freefly Tero</li>
                  <li style="margin-bottom: 0" class="btn">DJI Inspire 1</li>
                 </ul>
              </p>
              
               
		               
            </div><!-- end .cv-item -->
            <a href="#" id="contact"></a>
            
        </div><!-- end .span9 -->
      </div><!-- end .cv-section -->
    
      <div class="row cv-section dontprint">
        <div class="span3">
          <div class="cv-section-title">
            <h1><span>Contact Me<small>Rates &amp; Availability</small></span></h1>
              <p>phone: 214.636.1616</p>
          </div><!-- end .cv-section-title -->
        </div><!-- end .span3 -->
        <div class="span9">
            <div class="cv-item" data-ng-controller="contactController">
				<?php
				//if "email" variable is filled out, send email
				  if (isset($_REQUEST['email']))  {

				  //Email information
				  $admin_email = "shaungish@gmail.com";
				  $email = $_REQUEST['email'];
				  $subject = 'Shaungish.com email from - '.$_REQUEST['name'];
				  $comment = $_REQUEST['message'];

				  //send email
				  mail($admin_email, "$subject", $comment, "From:" . $email);

				  //Email response
				  echo "Thank you for contacting us!";
				  }

				  //if "email" variable is not filled out, display the form
				  else  {
				?>
              <form name="contactForm" novalidate id="contact" method="post" class="form" role="form">
             	
				<div class="row">
					
              		<div class="form-group">
              			<input class="span4 form-control" ng-model="name" id="name" name="name" placeholder="Name" type="text" required="">
              		</div>
              		
					<div class="form-group">
              			<input class="span4 form-control" ng-model="email" id="email" name="email" placeholder="Email" type="email" required="">
              		</div>
				</div>
              
				<textarea class="span4 form-control" ng-model="message" id="message" name="message" placeholder="Message" rows="5"></textarea>
              
			  	<div class="row">
              		<div class="span4 form-group">
              			<button class="btn btn-primary pull-right" type="submit">Submit</button>
              		</div>
            	</div>
			</form>
              
			<?php
			  }
			?>

            </div><!-- end .cv-item -->
        </div><!-- end .span9 -->
      </div><!-- end .cv-section -->
  	</div><!-- end #content -->
<div class="print print-footer">Gear list and complete resume available at ShaunGish.com</div>
</body>
</html>
